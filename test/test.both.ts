import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import * as api from '../ts/index.js';

let testSignupApiClient: api.SignupApiClient;

tap.test('should create a valid client', async () => {
  testSignupApiClient = new api.SignupApiClient();
  expect(testSignupApiClient).toBeInstanceOf(api.SignupApiClient);
});

tap.test('should get a public form', async () => {
  const signupForm = await testSignupApiClient.createSignupFormFromPublicToken('test_jhdf7656eruzhdf');
  const dataFields = await signupForm.getDataFields();
  console.log(signupForm);
  console.log(dataFields);
  await signupForm.sendFormEntry([
    { name: 'name', mandatory: true, type: 'String', value: 'Mustermann' },
    { name: 'firstName', mandatory: true, type: 'String', value: 'Max' },
    { name: 'email', mandatory: true, type: 'String', value: 'test@test.test' },
    { name: 'company', mandatory: true, type: 'String', value: 'Lossless GmbH' },
    { name: 'message', mandatory: true, type: 'String', value: 'hello there' },
  ]);
});

tap.start();
