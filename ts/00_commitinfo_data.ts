/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@signup.software/api',
  version: '1.0.10',
  description: 'the public api client for signup.software'
}
