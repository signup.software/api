import * as plugins from './api.plugins.js';
import { SignupApiClient } from './index.js';

export class SignupForm {
  public static async createFromPublicToken(
    signupApiClientRefArg: SignupApiClient,
    tokenArg: string
  ) {
    const getFormByToken =
      new plugins.typedrequest.TypedRequest<plugins.interfaces.requests.IRequestGetPublicFormByFormToken>(
        signupApiClientRefArg.apiUrl,
        'getPublicFormByFormToken'
      );
    const response = await getFormByToken.fire({
      formToken: tokenArg,
    });
    const signupForm = new SignupForm(signupApiClientRefArg, response.form);
    return signupForm;
  }

  public signupApiClientRefArg: SignupApiClient;
  public publicFormData: plugins.interfaces.data.IPublicForm;
  constructor(
    signupApiClientRefArg: SignupApiClient,
    publicFormDataArg: plugins.interfaces.data.IPublicForm
  ) {
    this.signupApiClientRefArg = signupApiClientRefArg;
    this.publicFormData = publicFormDataArg;
  }

  public async getDataFields(): Promise<plugins.interfaces.data.IPublicForm['dataFields']> {
    return this.publicFormData.dataFields;
  }

  public async sendFormEntry(dataFieldsArg: plugins.interfaces.data.IPublicFormEntry['dataFields']) {
    const sendFormDataRequest =
      new plugins.typedrequest.TypedRequest<plugins.interfaces.requests.IRequestSendPublicFormEntry>(
        this.signupApiClientRefArg.apiUrl,
        'sendPublicFormEntry'
      );
    const result = await sendFormDataRequest.fire({
      formEntry: {
        formToken: this.publicFormData.formToken,
        dataFields: dataFieldsArg,
        status: null,
        uniqueEntryId: null,
      }
    });

    return result;
  }
}
