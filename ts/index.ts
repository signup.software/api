import { SignupForm } from './api.classes.signupform.js';
import * as plugins from './api.plugins.js';

import * as interfaces from '@signup.software/interfaces';

export {
  interfaces
}

export class SignupApiClient {
  public apiUrl = 'https://api.signup.software/typedrequest';
  public authToken: string;
  public mode: 'public' | 'private';
  constructor(authTokenArg?: string) {
    this.mode = authTokenArg? 'private' : 'public';
    this.authToken = authTokenArg;
  }

  /**
   * note: this function can be called regardless of wether the client is authenticated or not.
   * @param publicFormTokenArg the public token of the form
   */
  public async createSignupFormFromPublicToken(publicFormTokenArg?: string) {
    const signupForm = await SignupForm.createFromPublicToken(this, publicFormTokenArg);
    return signupForm;
  }
}
